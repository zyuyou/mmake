#usage
- this file based on otp_src/lib/tools/src/make.erl, support the multiple processes compile.
- the exported functions are like make.erl, except for add the Worker Number as the first argument for every functions.

e.g. mmake:all(5), means you compile the codes defined in Emakefile by 5 workers.

in your Makefile or script :

<pre><code>erl -eval "case make:files([\"mmake.erl\"], [{outdir, \"ebin\"}]) of error -> halt(1); _ -> ok end" 
    -eval "case mmake:all(8,[$(MAKE_OPTS)]) of up_to_date -> halt(0); error -> halt(1) end."</code></pre>
    
# 使用
- 这个编译文件基于 otp_src/lib/tools/src/make.erl, 支持多进程编译。
- 模块的导出函数跟make.erl文件类似, 增加了Worker Number 作为每个函数的第一个参数。

举例：mmake:all(5), 指通过5个workers进程来编译你来的代码。
    
在你的Makefile、script或者.bat(windows下的批处理文件)中加入下述代码：

<pre><code>erl -eval "case make:files([\"mmake.erl\"], [{outdir, \"ebin\"}]) of error -> halt(1); _ -> ok end" 
    -eval "case mmake:all(8,[$(MAKE_OPTS)]) of up_to_date -> halt(0); error -> halt(1) end."</code></pre>
    
注意：通常mmake.erl的存放路径为： "deps/mmake/mmake.erl"